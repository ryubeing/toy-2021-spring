const fs = require("fs/promises");
const path = require("path");
const remark = require("remark");
const recommended = require("remark-preset-lint-recommended");
const html = require("remark-html");

function renderMD(file) {
  return new Promise((resolve, reject) => {
    remark()
      .use(recommended)
      .use(html)
      .process(file, function (err, file) {
        if (err instanceof Error) {
          reject(err);
        }
        resolve(String(file));
      });
  });
}

async function writeJsModule(fileName = "", rendered) {
  await fs.writeFile(
    path.join("./dist/", path.basename(fileName, ".md") + ".js"),
    `const html = \`${rendered}\`; export default html;`
  );
}

(async function main() {
  const fileName = process.argv[2];
  const mdFile = await fs.readFile(fileName);
  const rendered = await renderMD(mdFile);
  await writeJsModule(fileName, rendered);
  console.log("log", fileName, rendered);
})();
