import { useEffect, useRef } from 'react';
import CvApp from '../service/canvas';

type Props = {
  width: number;
  height: number;
};
const Canvas = ({ width = 300, height = 300 }: Props) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    if (canvasRef.current) {
      new CvApp(canvasRef.current);
    }
    return () => {};
  }, [canvasRef.current]);

  return <canvas style={{ width, height }} id="canvasApp" ref={canvasRef} />;
};

export default Canvas;
