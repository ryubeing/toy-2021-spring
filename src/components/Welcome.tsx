type props = {
  name: string;
  count: number;
};
const Welcome = ({ name, count }: props) => {
  return (
    <h1>
      Hello, {name} {count}
    </h1>
  );
};

export default Welcome;
