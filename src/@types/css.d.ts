declare module "*.css" {
  const styles: Record<string, string>;
  // 또는 const styles: { [key: string]: string };

  export default styles;
}

declare module "*.scss" {
  const styles: { [className: string]: string };
  export default styles;
}
