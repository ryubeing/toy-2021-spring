import { useState } from 'react';
import Welcome from './components/Welcome';
import css from './style/main.module.css';
import './style/main.scss';

import svg from './assets/react-1.svg';
import png from './assets/lion.png';
import Canvas from './components/Canvas';

const App = () => {
  const [count, setCount] = useState(0);

  const increment = () => {
    setCount(count + 1);
  };

  const decrement = () => {
    setCount(count - 1);
  };

  return (
    <div className={css.app}>
      <Canvas width={500} height={600} />
      <Welcome name="Sara" count={count} />
      <Welcome name="Cahal" count={count} />
      <Welcome name="Edite" count={count} />
      <div>
        <button onClick={increment} type="button">
          +
        </button>
        <button onClick={decrement} type="button">
          -
        </button>
      </div>
      <img src={svg} style={{ width: 300 }} alt="" />
      <img src={png} style={{ width: 300 }} alt="" />
      <div
        style={{
          width: 300,
          height: 300,
        }}
      />
    </div>
  );
};

export default App;
