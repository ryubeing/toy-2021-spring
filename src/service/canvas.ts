export default class CvApp {
  ctx: CanvasRenderingContext2D;
  num: number;
  constructor(canvas: HTMLCanvasElement) {
    this.ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
    this.num = 1;
    this.init();
  }

  init() {
    this.ctx.font = '30px Arial';
    this.ctx.fillText('Hello World', 10, 50);
    this.render();
  }
  render() {
    this.draw();
    requestAnimationFrame(this.render.bind(this));
  }
  draw() {
    this.num = this.num + 1;
    this.ctx.clearRect(0, 0, 300, 300);
    this.ctx.fillText(`${this.num}`, 10, 50);
  }
}
